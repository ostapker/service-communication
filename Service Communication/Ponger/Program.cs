﻿using System;
using System.Threading.Tasks;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var rabbitClient = new RabbitMQClient();
            Console.WriteLine("Ponger started!(Press any key to close.)");
            rabbitClient.Start();
            Console.ReadKey();
        }
    }
}
