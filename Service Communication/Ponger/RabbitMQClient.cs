﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ponger
{
    public class RabbitMQClient : IDisposable
    {
        private readonly MessageService _msgService;

        public RabbitMQClient()
        {
            _msgService = new MessageService();
        }

        public void Start()
        {
            StartWriteToQueue();
            ListenQueue();
        }

        public void StartWriteToQueue()
        {
            _msgService.StartWriteToQueue(
                    Properties.Resources.ExchangeName,
                    ExchangeType.Direct,
                    Properties.Resources.WriteQueueName,
                    Properties.Resources.WriteQueueKey
                    );
        }

        public void ListenQueue()
        {
            _msgService.ListenQueue(
                    Properties.Resources.ExchangeName,
                    ExchangeType.Direct,
                    Properties.Resources.ListenQueueName,
                    Properties.Resources.ListenQueueKey,
                    MessageReceivedAsync);
        }

        private async void MessageReceivedAsync(object sender, BasicDeliverEventArgs args)
        {
            this.ListenQueue();
            await Task.Delay(int.Parse(Properties.Resources.Delay));
            _msgService.SendMessageToQueue(Properties.Resources.Message);
        }

        public void Dispose()
        {
            _msgService.Dispose();
        }
    }
}
