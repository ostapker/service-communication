﻿using System;
using System.Threading.Tasks;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var rabbitClient = new RabbitMQClient();
            Console.WriteLine("Pinger started!(Press any key to close.)");
            rabbitClient.Start();
            Console.ReadKey();
        }
    }
}
