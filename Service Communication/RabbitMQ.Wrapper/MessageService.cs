﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class MessageService : IDisposable
    {
        private  IMessageConsumerScope _messageConsumerScope;
        private  IMessageProducerScope _messageProducerScope;

        private readonly IMessageConsumerScopeFactory _messageConsumerScopeFactory;
        private readonly IMessageProducerScopeFactory _messageProducerScopeFactory;


        public MessageService()
        {
            var connection = new QueueServices.ConnectionFactory(new Uri(Properties.Resources.Rabbit));

            _messageConsumerScopeFactory = new MessageConsumerScopeFactory(connection);
            _messageProducerScopeFactory = new MessageProducerScopeFactory(connection);
        }

        public void ListenQueue(string exchangeName, string exchangeType, string queueName, string routingKey, EventHandler<BasicDeliverEventArgs> received)
        {
            _messageConsumerScope = _messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = exchangeName,
                ExchangeType = exchangeType,
                QueueName = queueName,
                RoutingKey = routingKey
            });

            _messageConsumerScope.MessageConsumer.Received += MessageReceived;
            _messageConsumerScope.MessageConsumer.Received += received;
        }

        public void StartWriteToQueue(string exchangeName, string exchangeType, string queueName, string routingKey)
        {
            _messageProducerScope = _messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = exchangeName,
                ExchangeType = exchangeType,
                QueueName = queueName,
                RoutingKey = routingKey
            });
        }

        public bool SendMessageToQueue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(value);
                Console.WriteLine($"Sent {value} at {DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt")}");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                Console.WriteLine($"Received {value} at {DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt")}");

                processed = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                processed = false;
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }
        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}
